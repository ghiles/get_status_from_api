#!/usr/bin/env python
# -*- coding:Utf-8 -*-
__author__ = 'Ghiles KHEDDACHE'
__version__= 0.1

import requests
import json
import sys
import logging as log
from optparse import OptionParser


def exit_plugin(retcode, message=''):
    """ Exit gracefully with exitcode and (optional) message """

    log.debug('Exiting with status %s. Message: %s' % (retcode, message))
    
    if message:
        print (message)
    exit(retcode)


def get_token(url, user, passw, prox):

    auth_param = '{"username": "'+user+'", "password": "'+passw+'"}'

    payload = json.loads(auth_param)

    headers = {'content-type': 'application/json'}

    # get token
    r = requests.post(url+'/api/index.php?action=authenticate', data = payload, proxies=prox)

    response = r.json()

    return response['authToken']


def main():
    options = {}
    PROXY = ""

    options[('-u','--user')] = {'dest':'user',
                              'help':'username'}
    options[('-p','--password')] = {'dest':'password',
                              'help':'password'}
    options[('-a','--address')] = {'dest':'addr',
                              'help':'centreon url address'}
    options[('-H','--host')] = {'dest':'host',
                              'help':'search host'}
    options[('-s','--service')] = {'dest':'service',
                              'help':'search service'}

    options[('-x','--proxy')] = {'dest':'proxy',
                              'help':'search proxy'}

    parser = OptionParser()

    for param, option in options.items():
        parser.add_option(*param, **option)
    options, arguments = parser.parse_args()

    if options.addr is not None:
        CENTREON_URL = options.addr

    if options.user is not None:
        API_USER = options.user

    if options.password is not None:
        API_PWD = options.password

    if options.host is not None:
        HOST = options.host

    if options.service is not None:
        SERVICE = options.service

    if options.proxy is not None:
        PROXY = options.proxy
   
    PRX = dict()
    PRX['centreon-auth-token'] = PROXY

    try: 
    	authToken = get_token(CENTREON_URL, API_USER, API_PWD, PRX)
    except Exception, e: 
        msg = "[UNKNOWN] - Error : %s" % e
        exit_plugin(2, msg.strip('\r\n'))


    # construct req to get service status
    headers = dict()
    headers['centreon-auth-token'] = authToken
  
    # request get service api
    try:
    	r = requests.get(CENTREON_URL+'/api/index.php?object=centreon_realtime_services&action=list&search='+SERVICE , headers=headers, proxies=PRX)
    except Exception, e: 
    	msg = "[UNKNOWN] - Error : %s" % e
	exit_plugin(2,msg.strip('\r\n'))

    # read response

    data = r.json()
    state=int(data[0]["state"])
    description=data[0]["description"]
    perfdata=data[0]["perfdata"]
    output=data[0]["output"]
    msg=output+"|"+perfdata
   
    exit_plugin(state, msg)

if __name__ == '__main__':

#   log.basicConfig(level=log.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
   main()
